from sqlalchemy import text


class Storage:
    def __init__(self, sql_alchemy_session):
        self.conn = sql_alchemy_session

    def user_credentials(self, username, password):
        result = self.conn.execute(
            "SELECT COUNT(id) > 0 as value FROM users WHERE username=:username and password = MD5(:password)",
            {"username": username, "password": password})
        record = result.fetchone()
        valid = int(record[0])
        return valid

    def get_all_books(self):
        query = "SELECT * FROM books"
        result = self.conn.execute(query)
        records = result.fetchall()
        return records

    def delete_book(self, book_id):
        query = "DELETE FROM books WHERE id = :book_id"
        result = self.conn.execute(query, {"book_id": book_id})
        self.conn.commit()
        return result

    def insert_new_book(self, book_author, book_name, book_year):
        query = "INSERT INTO books (author, name, year) VALUES (:book_author, :book_name, :book_year)"
        result = self.conn.execute(query,
                                   {"book_author": book_author, "book_name": book_name, "book_year": book_year})
        self.conn.commit()
        return result

    def get_single_book(self, book_id):
        query = "SELECT * FROM books WHERE id = :book_id"
        result = self.conn.execute(query, {"book_id": book_id})
        record = result.fetchone()
        return record

    def update_single_book(self, book_author, book_name, book_year, book_id):
        query = "UPDATE books SET author = :book_author, name = :book_name, year = :book_year WHERE id = :book_id"
        result = self.conn.execute(query,
                                   {"book_author": book_author, "book_name": book_name, "book_year": book_year,
                                    "book_id": book_id})
        self.conn.commit()
        return result
