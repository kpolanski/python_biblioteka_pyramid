from pyramid.view import view_config
from pyramid.httpexceptions import HTTPFound

from biblioteka_pyramid.Storage import Storage


@view_config(route_name='home', renderer='templates/login.jinja2')
def home(request):
    session = request.session
    if not session.get('logged_in'):
        return {}
    else:
        url = request.route_url('list_books')
        return HTTPFound(location=url)


@view_config(route_name='login', renderer='templates/login.jinja2')
def do_admin_login(request):
    session = request.session
    session['test'] = True
    db = Storage(request.db)
    if db.user_credentials(request.POST['username'], request.POST['password']) == 1:
        session['logged_in'] = True
    else:
        request.session.flash('Błędne hasło lub użytkownik')
    url = request.route_url('home')
    return HTTPFound(location=url)


@view_config(route_name='logout')
def logout(request):
    session = request.session
    session['logged_in'] = False
    url = request.route_url('home')
    return HTTPFound(location=url)


@view_config(route_name='list_books', renderer='templates/books.jinja2')
def list_books(request):
    session = request.session
    if 'logged_in' in session and session['logged_in'] == False:
        url = request.route_url('home')
        return HTTPFound(location=url)
    db = Storage(request.db)
    books = db.get_all_books()
    return {"books": books}


@view_config(route_name='delete_book')
def delete_book(request):
    session = request.session
    book_id = request.matchdict['book_id']
    if 'logged_in' in session and session['logged_in'] == False:
        return HTTPFound(location=request.route_url('home'))
    db = Storage(request.db)
    db.delete_book(book_id)
    request.session.flash("Książka usunięta", "success")
    return HTTPFound(location=request.route_url('list_books'))


@view_config(route_name='add_book', renderer='templates/add_book.jinja2')
def add_book_form(request):
    session = request.session
    if 'logged_in' in session and session['logged_in'] == False:
        return HTTPFound(location=request.route_url('home'))
    return {}


@view_config(route_name='add_book_post')
def add_book(request):
    session = request.session
    if 'logged_in' in session and session['logged_in'] == False:
        return HTTPFound(location=request.route_url('home'))
    db = Storage(request.db)
    db.insert_new_book(book_author=request.POST['book_author'],
                       book_name=request.POST['book_name'],
                       book_year=request.POST['book_year'])
    request.session.flash("Dodano nową książkę. Możesz dodawać dalej.", "success")
    return HTTPFound(location=request.route_url('add_book'))


@view_config(route_name='edit_book', renderer='templates/edit_book.jinja2')
def edit_book_form(request):
    session = request.session
    book_id = request.matchdict['book_id']
    if 'logged_in' in session and session['logged_in'] == False:
        return HTTPFound(location=request.route_url('home'))
    db = Storage(request.db)
    book = db.get_single_book(book_id=book_id)

    return {"book": book}


@view_config(route_name='edit_book_post')
def edit_book(request):
    session = request.session
    if 'logged_in' in session and session['logged_in'] == False:
        return HTTPFound(location=request.route_url('home'))
    db = Storage(request.db)
    db.update_single_book(book_author=request.POST['book_author'],
                    book_name=request.POST['book_name'],
                    book_year=request.POST['book_year'],
                    book_id=request.POST['book_id'])
    request.session.flash("Książka zmodyfikowana.", "success")
    return HTTPFound(location=request.route_url('list_books'))
